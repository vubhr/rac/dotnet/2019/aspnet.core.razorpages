﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Demo.App.Models;
using Demo.App.Persistence;
using Demo.App.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.Logging;

namespace Demo.App.Pages
{
    public class IndexModel : PageModel
    {
        private readonly ILogger<IndexModel> _logger;
        private readonly IProductRepository _products;
        private readonly IProductsService _productsService;

        [TempData]
        public string VrstaPoruke { get; set; }

        [TempData]
        public string Poruka { get; set; }
        public List<Product> Proizvodi { get; set; }

        [BindProperty]
        public Product Proizvod { get; set; }

        public IndexModel(ILogger<IndexModel> logger, 
            IProductRepository products, IProductsService productsService)
        {
            _logger = logger;
            _products = products;
            _productsService = productsService;

            Proizvodi = productsService.GetProductsAsync().Result;
        }

  
        public void OnGet()
        {

        }

        public async Task<IActionResult> OnPostAddProduct()
        {
            if(!ModelState.IsValid)
            {
                VrstaPoruke = "danger";
                Poruka = "Neispravan unos!";

                return Page();
            }

            try
            {
                await _productsService.CreateProduct(Proizvod);
            }
            catch(Exception ex)
            {
                Console.WriteLine($"Dogodila se pogreška tokom dodavanja proizvoda:{ex}");
                VrstaPoruke = "error";
                Poruka = "Dogodila se pogreška prilikom dodavanja proizvoda!";
                return Page();
            }

            VrstaPoruke = "success";
            Poruka = "Uspješno dodan proizvod!";
            return RedirectToPage("Index");
        }

        public void OnGetEditProduct(int indeks)
        {
            var proizvod = Proizvodi[indeks];
            Proizvod = proizvod;
        }

        public IActionResult OnPostUpdateProduct(int id)
        {
            if (!ModelState.IsValid)
            {
                // Ispiši mi neku grešku
                Poruka = "Neispravan unos!";
                return Page();
            }

            //var product = _db.Products.Where(p => p.Id == id).First();
            //product.Image = Proizvod.Image;
            //product.Name = Proizvod.Name;
            //product.Price = Proizvod.Price;

            //_db.Products.Update(product);
            //_db.SaveChanges();

            //_db.Entry(Proizvod).State = Microsoft.EntityFrameworkCore.EntityState.Modified;
            //_db.SaveChanges();

            Poruka = "Uspješno ažuriran proizvod!";
            VrstaPoruke = "success";

            return RedirectToPage("Index");
        }


        public IActionResult OnGetDeleteProduct(int id)
        {
            //var product = _db.Products.Where(p => p.Id == id).First();
            //try
            //{
                
            //    _db.Products.Remove(product);
            //    _db.SaveChanges();
            //}
            //catch(Exception ex)
            //{
            //    Poruka = $"Dogodila se pogreška prilikom brisanja proizvoda: {product.Name}!";
            //    _logger.LogError(ex, Poruka);
                
            //    VrstaPoruke = "danger";
            //    return Page();
            //}

            //Poruka = $"Uspješno obrisan proizvod {product.Name}!";
            VrstaPoruke = "success";

            return RedirectToPage("Index");
        }
    }
}
