﻿using Demo.Api.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Demo.Api.Controllers
{
    [Route("[controller]")]
    public class ProductsController : ControllerBase
    {
        private readonly SqlLiteDbContext _context;
        public ProductsController(SqlLiteDbContext context)
        {
            _context = context;
        }

        [HttpGet]
        public List<Product> OnGet()
        {
            return _context.Products.OrderByDescending(p => p.Id).Take(2).ToList();
        }

        [HttpPost]
        public void OnPost([FromBody]Product product)
        {
            if(!ModelState.IsValid)
            {
                throw new Exception("Ne valja ti proizvod!");
            }

            _context.Products.Add(product);
            _context.SaveChanges();
        }

        
    }
}
