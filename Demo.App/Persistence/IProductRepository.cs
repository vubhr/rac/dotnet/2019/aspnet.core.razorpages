﻿using Demo.App.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Demo.App.Persistence
{
    public interface IProductRepository
    {
        List<Product> GetAll();
        void AddProduct(Product product);

        void EditProduct(Product product);
    }
}
