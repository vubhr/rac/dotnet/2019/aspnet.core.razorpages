﻿using Demo.App.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Demo.App.Services
{
    public interface IProductsService
    {
        Task<List<Product>> GetProductsAsync();
        Task<Product> CreateProduct(Product product);
    }
}