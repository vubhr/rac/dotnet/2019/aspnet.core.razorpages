﻿using Demo.App.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Demo.App.Persistence
{
    public class InMemoryProductRepository : IProductRepository
    {
        private List<Product> _products { get; set; } = new List<Product>();

        public void AddProduct(Product product)
        {
            _products.Add(product);
        }

        public void EditProduct(Product product)
        {
            int indeks = 0;
            for(var i = 0; i < _products.Count; i++)
            {
                if(_products[i].Name == product.Name)
                {
                    indeks = i;
                    break;
                }
            }

            _products[indeks] = product;
        }

        public List<Product> GetAll()
        {
            return _products;
        }


    }
}
