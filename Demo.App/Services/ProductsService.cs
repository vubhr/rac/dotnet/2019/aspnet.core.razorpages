﻿using Demo.App.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace Demo.App.Services
{
    public class ProductsService : IProductsService
    {
        private readonly IHttpClientFactory _contextFactory;
        private readonly IConfiguration _config;

        public ProductsService(IHttpClientFactory contextFactory, IConfiguration config)
        {
            _contextFactory = contextFactory;
            _config = config;
        }

        public async Task<Product> CreateProduct(Product product)
        {
            var client = _contextFactory.CreateClient();
            var content = new StringContent(
                    JsonConvert.SerializeObject(product), 
                    Encoding.UTF8, 
                    "application/json"
            );

            // Doraditi da se POST-a na naš API na /products endpoint
            var response = await client.PostAsync($"{_config.GetValue<string>("DemoApiUrl")}/products", content);
            response.EnsureSuccessStatusCode();

            //var products = JsonConvert
            //    .DeserializeObject<List<Product>>(
            //    await response.Content.ReadAsStringAsync());
            //return products;

            return null;
        }

        public async Task<List<Product>> GetProductsAsync()
        {
            var client = _contextFactory.CreateClient();
            var response = await client.GetAsync(
                $"{_config.GetValue<string>("DemoApiUrl")}/products");
            response.EnsureSuccessStatusCode();

            var products = JsonConvert
                .DeserializeObject<List<Product>>(
                await response.Content.ReadAsStringAsync());
            return products;
        }
    }
}
