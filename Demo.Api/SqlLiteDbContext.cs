﻿using Demo.Api.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Demo.Api
{
    public class SqlLiteDbContext : DbContext
    {
        private readonly IConfiguration _config;
        public DbSet<Product> Products { get; set; }

        public SqlLiteDbContext(IConfiguration config)
        {
            _config = config;
        }
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlite(_config.GetConnectionString("DbConnectionString"));
        }
    }
}
