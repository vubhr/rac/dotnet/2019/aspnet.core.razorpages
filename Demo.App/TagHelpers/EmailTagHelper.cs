﻿using Microsoft.AspNetCore.Razor.TagHelpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Demo.App.TagHelpers
{
    [HtmlTargetElement("email")]
    public class EmailTagHelper : TagHelper
    {
        public string MailTo { get; set; } 
        public string Domain { get; set; }
        public string Title { get; set; }

        public override void Process(TagHelperContext context, TagHelperOutput output)
        {
            output.TagName = "a";
            var adresa = $"{MailTo}@{Domain}";
            output.Attributes.Add("href", $"mailto:{adresa}");
            output.Content.SetContent(adresa);
        }
    }
}
